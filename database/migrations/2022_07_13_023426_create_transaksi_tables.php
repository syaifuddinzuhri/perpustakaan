<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_transaksi', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_user_id')->nullable();
            $table->string('no_invoice');
            $table->timestamp('tanggal_pinjam')->nullable();
            $table->timestamp('estimasi_kembali')->nullable();
            $table->timestamp('tanggal_pengembalian')->nullable();
            $table->integer('jumlah_hari_pinjam')->nullable();
            $table->integer('jumlah_telat')->nullable();
            $table->float('total_denda', 16, 2)->nullable();
            $table->enum('status', ['dipinjam', 'dikembalikan'])->default('dipinjam');
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('deleted_by')->default(0);
            $table->timestamps();
            $table->softDeletes(); // Generate deleted_at

            $table->index('no_invoice');
            $table->index('tanggal_pinjam');
            $table->index('estimasi_kembali');
            $table->index('tanggal_pengembalian');
            $table->index('total_denda');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_transaksi');
    }
}
