<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_buku', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_rak_buku_id')->nullable();
            $table->bigInteger('m_kategori_buku_id')->nullable();
            $table->string('judul');
            $table->string('penulis')->nullable();
            $table->string('isbn')->nullable();
            $table->string('penerbit')->nullable();
            $table->integer('tahun_terbit')->nullable();
            $table->text('deskripsi')->nullable();
            $table->integer('jumlah')->default(0);
            $table->string('gambar')->nullable();
            $table->enum('status', [1, 0])->default(0);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('deleted_by')->default(0);
            $table->timestamps();
            $table->softDeletes(); // Generate deleted_at
            $table->index('judul');
            $table->index('penulis');
            $table->index('penerbit');
            $table->index('tahun_terbit');
            $table->index('jumlah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_buku');
    }
}
