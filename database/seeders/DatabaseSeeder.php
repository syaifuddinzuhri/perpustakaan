<?php

namespace Database\Seeders;

use App\Models\Master\SettingModel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        SettingModel::create([
            'param' => 'denda',
            'value' => 1000,
        ]);
        $this->call([
            UserSeeder::class
        ]);
    }
}
