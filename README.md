## Cara Instalasi

- Clone repository [https://gitlab.com/syaifuddinzuhri/perpustakaan](https://gitlab.com/syaifuddinzuhri/perpustakaan)
- Masuk ke folder **perpustakaan** dan jalankan perintah **composer install**
- Masuk ke folder **perpustakaan/resources/angular** dan jalankan perintah **npm install**
- Pada root Folder **perpustakaan** buat sebuah file dengan nama **.env** dan copy konten dari file **.env.example** ke dalam file tersebut
- Sesuaikan pengaturan koneksi database pada file **.env** (Key yang perlu disesuaikan DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD)
- Install Redis, tutorial bisa dibaca di [https://redis.io/topics/quickstart](https://redis.io/topics/quickstart)
- Pada terminal jalankan perintah **php artisan migrate:fresh --seed** untuk migrasi tabel yang diperlukan ke database dan menjalankan database seeder
- Pada terminal jalankan perintah **php artisan serve --port=8000** 
- Laravel bisa diakses menggunakan url **http://localhost:8000/**
