<?php

use App\Http\Controllers\Api\Master\BukuController;
use App\Http\Controllers\Api\User\AuthController;
use App\Http\Controllers\Api\User\RoleController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Api\Master\CustomerController;
use App\Http\Controllers\Api\Master\ItemController;
use App\Http\Controllers\Api\Master\KategoriBukuController;
use App\Http\Controllers\Api\Master\RakBukuController;
use App\Http\Controllers\Api\Master\TransaksiController;
use App\Http\Controllers\Master\LaporanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    /**
     * CRUD user
     */
    Route::get('/users', [UserController::class, 'index'])->middleware(['web', 'auth.api:user_view']);
    Route::get('/users/{id}', [UserController::class, 'show'])->middleware(['web', 'auth.api:user_view']);
    Route::post('/users', [UserController::class, 'store'])->middleware(['web', 'auth.api:user_create']);
    Route::put('/users', [UserController::class, 'update'])->middleware(['web', 'auth.api:user_update']);
    Route::delete('/users/{id}', [UserController::class, 'destroy'])->middleware(['web', 'auth.api:user_delete']);

    /**
     * CRUD role / hak akses
     */
    Route::get('/roles', [RoleController::class, 'index'])->middleware(['web', 'auth.api:roles_view']);
    Route::get('/roles/{id}', [RoleController::class, 'show'])->middleware(['web', 'auth.api:roles_view']);
    Route::post('/roles', [RoleController::class, 'store'])->middleware(['web', 'auth.api:roles_create']);
    Route::put('/roles', [RoleController::class, 'update'])->middleware(['web', 'auth.api:roles_update']);
    Route::delete('/roles/{id}', [RoleController::class, 'destroy'])->middleware(['web', 'auth.api:roles_delete']);

    /**
     * CRUD rak buku
     */
    Route::get('/rak-buku', [RakBukuController::class, 'index'])->middleware(['web', 'auth.api:rak_buku_view']);
    Route::get('/rak-buku/{id}', [RakBukuController::class, 'show'])->middleware(['web', 'auth.api:rak_buku_view']);
    Route::post('/rak-buku', [RakBukuController::class, 'store'])->middleware(['web', 'auth.api:rak_buku_create']);
    Route::put('/rak-buku', [RakBukuController::class, 'update'])->middleware(['web', 'auth.api:rak_buku_update']);
    Route::delete('/rak-buku/{id}', [RakBukuController::class, 'destroy'])->middleware(['web', 'auth.api:rak_buku_delete']);

    /**
     * CRUD kategori buku
     */
    Route::get('/kategori-buku', [KategoriBukuController::class, 'index'])->middleware(['web', 'auth.api:kategori_buku_view']);
    Route::get('/kategori-buku/{id}', [KategoriBukuController::class, 'show'])->middleware(['web', 'auth.api:kategori_buku_view']);
    Route::post('/kategori-buku', [KategoriBukuController::class, 'store'])->middleware(['web', 'auth.api:kategori_buku_create']);
    Route::put('/kategori-buku', [KategoriBukuController::class, 'update'])->middleware(['web', 'auth.api:kategori_buku_update']);
    Route::delete('/kategori-buku/{id}', [KategoriBukuController::class, 'destroy'])->middleware(['web', 'auth.api:kategori_buku_delete']);

    /**
     * CRUD buku
     */
    Route::get('/buku', [BukuController::class, 'index'])->middleware(['web', 'auth.api:buku_view']);
    Route::get('/buku/{id}', [BukuController::class, 'show'])->middleware(['web', 'auth.api:buku_view']);
    Route::post('/buku', [BukuController::class, 'store'])->middleware(['web', 'auth.api:buku_create']);
    Route::put('/buku', [BukuController::class, 'update'])->middleware(['web', 'auth.api:buku_update']);
    Route::delete('/buku/{id}', [BukuController::class, 'destroy'])->middleware(['web', 'auth.api:buku_delete']);

    /**
     * CRUD transaksi
     */
    Route::get('/transaksi', [TransaksiController::class, 'index'])->middleware(['web', 'auth.api:transaksi_view']);
    Route::get('/transaksi/{id}', [TransaksiController::class, 'show'])->middleware(['web', 'auth.api:transaksi_view']);
    Route::post('/transaksi/peminjaman', [TransaksiController::class, 'store'])->middleware(['web', 'auth.api:transaksi_create']);
    Route::post('/transaksi/pengembalian', [TransaksiController::class, 'storePengembalian'])->middleware(['web', 'auth.api:transaksi_create']);
    Route::put('/transaksi/peminjaman', [TransaksiController::class, 'update'])->middleware(['web', 'auth.api:transaksi_update']);
    Route::put('/transaksi/pengembalian', [TransaksiController::class, 'updatePengembalian'])->middleware(['web', 'auth.api:transaksi_update']);
    Route::delete('/transaksi/{id}', [TransaksiController::class, 'destroy'])->middleware(['web', 'auth.api:transaksi_delete']);

    Route::get('/transaksi-user', [TransaksiController::class, 'getTransaksiUser'])->middleware(['web', 'auth.api:transaksi_view']);

    Route::get('/laporan/dashboard', [LaporanController::class, 'laporanDashboard']);

    /**
     * Route khusus authentifikasi
     */
    Route::prefix('auth')->group(function () {
        Route::post('/login', [AuthController::class, 'login']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::get('/profile', [AuthController::class, 'profile'])->middleware(['auth.api']);
        Route::get('/csrf', [AuthController::class, 'csrf'])->middleware(['web']);
    });
});

Route::get('/', function () {
    return response()->failed(['Endpoint yang anda minta tidak tersedia']);
});

/**
 * Jika Frontend meminta request endpoint API yang tidak terdaftar
 * maka akan menampilkan HTTP 404
 */
Route::fallback(function () {
    return response()->failed(['Endpoint yang anda minta tidak tersedia']);
});
