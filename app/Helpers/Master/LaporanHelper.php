<?php

namespace App\Helpers\Master;

use App\Models\Master\TransaksiModel;
use Carbon\Carbon;

class LaporanHelper
{

    public function laporanDashboard()
    {
        $laporan = [];
        $now = Carbon::now();
        $yesterday = Carbon::yesterday();
        $laporan['dipinjam_hari_ini'] = TransaksiModel::where('status', 'dipinjam')->where('tanggal_pinjam', '=', $now->toDateString())->count('*');
        $laporan['dikembalikan_hari_ini'] = TransaksiModel::where('status', 'dikembalikan')->where('tanggal_pinjam', '=', $now->toDateString())->count('*');
        $laporan['dipinjam_hari_kemarin'] = TransaksiModel::where('status', 'dipinjam')->where('tanggal_pinjam', '=', $yesterday->toDateString())->count('*');
        $laporan['dikembalikan_hari_kemarin'] = TransaksiModel::where('status', 'dikembalikan')->where('tanggal_pinjam', '=', $yesterday->toDateString())->count('*');
        $laporan['dipinjam_bulan_ini'] = TransaksiModel::where('status', 'dipinjam')->whereMonth('tanggal_pinjam', '=', $now->format('m'))->count('*');
        $laporan['dikembalikan_bulan_ini'] = TransaksiModel::where('status', 'dikembalikan')->whereMonth('tanggal_pinjam', '=', $now->format('m'))->count('*');
        $laporan['dipinjam_bulan_lalu'] = TransaksiModel::where('status', 'dipinjam')->whereMonth('tanggal_pinjam', '=', $now->subMonth()->format('m'))->count('*');
        $laporan['dikembalikan_bulan_lalu'] = TransaksiModel::where('status', 'dikembalikan')->whereMonth('tanggal_pinjam', '=', $now->subMonth()->format('m'))->count('*');
        return $laporan;
    }
}
