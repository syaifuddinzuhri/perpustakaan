<?php

namespace App\Helpers\Master;

use App\Models\Master\BukuModel;
use App\Repository\DetailInterface;
use App\Models\Master\TransaksiDetModel;
use App\Models\Master\TransaksiModel;
use Illuminate\Support\Facades\DB;

/**
 * Helper untuk manajemen detail transaksi
 * Mengambil data, menambah, mengubah, & menghapus ke tabel t_transaksi
 *
 * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
 */
class TransaksiDetHelper implements DetailInterface
{
    private $model;
    private $parent;
    protected $bukuModel;

    public function __construct(TransaksiModel $item)
    {
        $this->parent = $item;
        $this->model = new TransaksiDetModel();
        $this->bukuModel = new BukuModel();
    }

    /**
     * Mempersiapkan data baru untuk diinput ke tabel t_transaksi_det
     *
     * @param  array $detail
     * $detail['id']
     * $detail['t_transaksi_id']
     * $detail['m_buku_id']
     * @param  int $itemId id dari tabel t_transaksi
     *
     * @return object
     */
    public function prepare(array $detail): array
    {
        $arrDetail = [];
        foreach ($detail as $key => $val) {
            $arrDetail[$key]['t_transaksi_id'] = $this->parent->id;
            $arrDetail[$key]['m_buku_id'] = $val;
        }

        return $arrDetail;
    }

    /**
     * Fungsi untuk mengambil detail item berdasarkan item id
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param  int $itemId
     *
     * @return object
     */
    public function getAll(): object
    {
        return $this->model->getAll($this->parent->id);
    }

    /**
     * Fungsi untuk melakukan grouping detail item berdasarkan id tabel t_transaksi_det
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @return array
     */
    public function groupById(): array
    {
        $detailItems = $this->getAll();
        $arrDetail = [];
        foreach ($detailItems as $val) {
            $arrDetail[$val] = [
                'id' => $val['id'],
                't_transaksi_id' => $val['t_transaksi_id'],
                'm_buku_id' => $val['m_buku_id'],
            ];
        }

        return $arrDetail;
    }

    /**
     * Fungsi untuk mengecek jika ada perubahan data detail
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param  array $oldDetail
     * @param  array $newDetail
     *
     * @return boolean
     */
    public function isChanged(array $oldDetail, array $newDetail): bool
    {
        return false;
    }

    /**
     * Proses mass input data detail ke tabel m_user_det
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $newDetail array multidimensi
     * $newDetail[x]['t_transaksi_id']
     * $newDetail[x]['m_buku_id']
     *
     * @return boolean
     */
    public function create(array $detail): bool
    {
        if (count($detail) > 0) {
            // Insert data ke tabel t_transaksi_det
            $newDetail = $this->prepare($detail);
            $this->model->store((array) $newDetail);

            // Update jumlah buku
            foreach ($detail as $key => $value) {
                $buku = $this->bukuModel->getById($value);
                $payloadUpdateBuku = [
                    'jumlah' => $buku->jumlah - 1
                ];
                $this->bukuModel->edit($payloadUpdateBuku, $value);
            }
            return true;
        }
        return false;
    }

    /**
     * Fungsi untuk melakukan update detail item
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $newDetail
     * $newDetail[x]['id']
     * $newDetail[x]['t_transaksi_id']
     * $newDetail[x]['keterangan']
     * $newDetail[x]['tipe']
     * $newDetail[x]['harga']
     *
     * @return boolean
     */
    public function update(array $newDetail): bool
    {
        try {
            DB::beginTransaction();
            $this->updateDel(true);
            $this->create($newDetail);

            DB::commit();
            return true;
        } catch (\Throwable $th) {
            DB::rollback();
            return false;
        }
    }

    public function updateDel($del)
    {
        $old = TransaksiDetModel::where('t_transaksi_id', $this->parent->id)->get();
        foreach ($old as $key => $value) {
            $buku = $this->bukuModel->getById($value->m_buku_id);
            $payloadUpdateBukuLama = [
                'jumlah' => $buku->jumlah + 1
            ];
            $res = $this->bukuModel->edit($payloadUpdateBukuLama, $buku->id);
            if ($del) $value->delete();
        }
    }

    /**
     * Hapus detail item yang tidak digunakan
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param  array $usedDetailId id detail yang tetap digunakan / disimpan
     * @return void
     */
    public function deleteUnUsed(array $arrOldDet): void
    {
        $unUsedId = [];
        foreach ($arrOldDet as $val) {
            $unUsedId[] = $val['id'];
        }

        if (!empty($unUsedId)) {
            $this->model->deleteUnUsed($unUsedId, $this->parent->id);
        }
    }
}
