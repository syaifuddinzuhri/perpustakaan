<?php

namespace App\Helpers\Master;

use App\Models\Master\BukuModel;
use App\Repository\CrudInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Helper untuk manajemen buku
 * Mengambil data, menambah, mengubah, & menghapus ke tabel m_buku
 *
 * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
 */
class BukuHelper implements CrudInterface
{
    protected $bukuModel;

    public function __construct()
    {
        $this->bukuModel = new BukuModel();
    }

    /**
     * Mengambil data item dari tabel m_buku
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param  array $filter
     * $filter['nama'] = string
     * $filter['penulis'] = string
     * $filter['penerbit'] = string
     * $filter['tahun_terbit'] = string
     * @param integer $itemPerPage jumlah data yang tampil dalam 1 halaman, kosongi jika ingin menampilkan semua data
     * @param string $sort nama kolom untuk melakukan sorting mysql beserta tipenya DESC / ASC
     *
     * @return object
     */
    public function getAll(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        return $this->bukuModel->getAll($filter, $itemPerPage, $sort);
    }

    /**
     * Mengambil 1 data item dari tabel m_buku
     *
     * @param  integer $id id dari tabel m_buku
     * @return object
     */
    public function getById(int $id): object
    {
        return $this->bukuModel->getById($id);
    }

    /**
     * method untuk menginput data baru ke tabel m_buku
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $payload
     * $payload['nama'] = string
     * $payload['penulis] = string
     * $payload['penerbit] = string
     * $payload['tahun_terbit] = number
     * $payload['gambar] = string
     * $payload['deskripsi] = string
     *
     * @return void
     */
    public function create(array $payload): array
    {
        try {
            if (!empty($payload['gambar'])) {
                // $gambar = $payload['gambar']->store('upload/gambarBuku', 'public');
                // $payload['gambar'] = $gambar;

                $folderPath = "upload/gambarBuku";
                $image_parts = explode(";base64,", $payload['gambar']);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = $folderPath . '/' . uniqid() . '.' . $image_type;

                Storage::disk('public')->put($file, $image_base64);
                $payload['gambar'] = $file;
            }
            $payload['m_kategori_buku_id'] = $payload['kategori']['id'];
            $payload['m_rak_buku_id'] = $payload['rak']['id'];
            unset($payload['kategori']);
            unset($payload['rak']);
            $payload['tahun_terbit'] = $payload['tahun_terbit']['value'];
            $newItem = $this->bukuModel->store($payload);

            return [
                'status' => true,
                'data' => $newItem
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    /**
     * method untuk mengubah item pada tabel m_buku
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $payload
     * $payload['nama'] = string
     * $payload['penulis] = string
     * $payload['penerbit] = string
     * $payload['tahun_terbit] = number
     * $payload['gambar] = string
     * $payload['deskripsi] = string
     * $payload['is_verified] = string
     *
     * @return array
     */
    public function update(array $payload, int $id): array
    {
        try {
            if (!empty($payload['gambar'])) {
                // $gambar = $payload['gambar']->store('upload/gambarBuku', 'public');
                // $payload['gambar'] = $gambar;
                $folderPath = "upload/gambarBuku";
                $image_parts = explode(";base64,", $payload['gambar']);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = $folderPath . '/' . uniqid() . '.' . $image_type;

                Storage::disk('public')->put($file, $image_base64);
                $payload['gambar'] = $file;
            } else {
                unset($payload['gambar']); // Jika gambar kosong, hapus dari array agar tidak diupdate
            }
            if (is_array($payload['tahun_terbit'])) {
                $payload['tahun_terbit'] = $payload['tahun_terbit']['value'];
            }
            if (is_array($payload['kategori'])) {
                $payload['m_kategori_buku_id'] = $payload['kategori']['id'];
            }
            if (is_array($payload['rak'])) {
                $payload['m_rak_buku_id'] = $payload['rak']['id'];
            }
            $updateBuku = $this->bukuModel->edit($payload, $id);
            $dataBuku = $this->getById($id);

            return [
                'status' => true,
                'data' => $dataBuku
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    /**
     * Menghapus data item dengan sistem "Soft Delete"
     * yaitu mengisi kolom deleted_at agar data tsb tidak
     * keselect waktu menggunakan Query
     *
     * @param  integer $id id dari tabel m_buku
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        try {
            $this->bukuModel->drop($id);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
}
