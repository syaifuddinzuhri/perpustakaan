<?php

namespace App\Helpers\Master;

use App\Models\Master\SettingModel;
use App\Repository\CrudInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Helper untuk manajemen buku
 * Mengambil data, menambah, mengubah, & menghapus ke tabel m_buku
 *
 * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
 */
class SettingHelper
{
    protected $settingModel;

    public function __construct()
    {
        $this->settingModel = new SettingModel();
    }

    /**
     * Mengambil data item dari tabel m_buku
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param  array $filter
     * $filter['nama'] = string
     * $filter['penulis'] = string
     * $filter['penerbit'] = string
     * $filter['tahun_terbit'] = string
     * @param integer $itemPerPage jumlah data yang tampil dalam 1 halaman, kosongi jika ingin menampilkan semua data
     * @param string $sort nama kolom untuk melakukan sorting mysql beserta tipenya DESC / ASC
     *
     * @return object
     */
    public function getAll(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        return $this->settingModel->getAll($filter, $itemPerPage, $sort);
    }

    /**
     * Mengambil 1 data item dari tabel m_buku
     *
     * @param  integer $id id dari tabel m_buku
     * @return object
     */
    public function getById(int $id): object
    {
        return $this->settingModel->getById($id);
    }

    /**
     * Mengambil 1 data item dari tabel m_buku
     *
     * @param  integer $id id dari tabel m_buku
     * @return object
     */
    public function getByParam(string $value): object
    {
        return $this->settingModel->getByParam($value);
    }

    /**
     * method untuk mengubah item pada tabel m_buku
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $payload
     * $payload['value'] = string
     *
     * @return array
     */
    public function update(array $payload, int $id): array
    {
        try {
            $updateBuku = $this->settingModel->edit($payload, $id);
            $dataBuku = $this->getById($id);

            return [
                'status' => true,
                'data' => $dataBuku
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }
}
