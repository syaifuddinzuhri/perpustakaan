<?php

namespace App\Helpers\Master;

use App\Models\Master\BukuModel;
use App\Models\Master\SettingModel;
use App\Models\Master\TransaksiModel;
use App\Repository\CrudInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Helper untuk manajemen peminjaman
 * Mengambil data, menambah, mengubah, & menghapus ke tabel t_transaksi
 *
 * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
 */
class TransaksiHelper implements CrudInterface
{
    protected $transaksiModel;
    protected $settingModel;
    protected $bukuModel;

    public function __construct()
    {
        $this->transaksiModel = new TransaksiModel();
        $this->settingModel = new SettingModel();
        $this->bukuModel = new BukuModel();
    }

    /**
     * Mengambil data item dari tabel t_transaksi
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param  array $filter
     * @param integer $itemPerPage jumlah data yang tampil dalam 1 halaman, kosongi jika ingin menampilkan semua data
     * @param string $sort nama kolom untuk melakukan sorting mysql beserta tipenya DESC / ASC
     *
     * @return object
     */
    public function getAll(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        return $this->transaksiModel->getAll($filter, $itemPerPage, $sort);
    }

    /**
     * Mengambil data item dari tabel t_transaksi
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param  array $filter
     * @param integer $itemPerPage jumlah data yang tampil dalam 1 halaman, kosongi jika ingin menampilkan semua data
     * @param string $sort nama kolom untuk melakukan sorting mysql beserta tipenya DESC / ASC
     *
     * @return object
     */
    public function getTransaksiUser(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        return $this->transaksiModel->getAll($filter, $itemPerPage, $sort);
    }

    /**
     * Mengambil 1 data item dari tabel t_transaksi
     *
     * @param  integer $id id dari tabel t_transaksi
     * @return object
     */
    public function getById(int $id): object
    {
        return $this->transaksiModel->getById($id);
    }

    /**
     * method untuk menginput data baru ke tabel t_transaksi
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $payload
     * $payload['Peminjaman] = string
     * $payload['user] = string
     * $payload['tanggal_mulai] = date
     * $payload['tanggal_selesai] = date
     *
     * @return void
     */
    public function create(array $payload): array
    {
        try {
            DB::beginTransaction();
            $detailItem = $payload['buku'] ?? [];

            $payload['m_user_id'] = $payload['user']['id'];
            $payload['status'] = 'dipinjam';
            $payload['no_invoice'] = 'INV' . time() . rand(0, 999);
            $parse = Carbon::parse($payload['tanggal_pinjam']);
            $payload['tanggal_pinjam'] = Carbon::parse($payload['tanggal_pinjam']);
            $payload['estimasi_kembali'] = $parse->addDays($payload['jumlah_hari_pinjam'] + 1);
            unset($payload['buku']);
            unset($payload['user']);
            if ($payload['estimasi_kembali'] < Carbon::now()) {
                $denda = $this->settingModel->getByParam('denda');
                $payload['jumlah_telat'] = $payload['estimasi_kembali']->diffInDays(Carbon::now());
                $payload['total_denda'] = $payload['jumlah_telat'] * $denda->value;
            }
            $newItem = $this->transaksiModel->store($payload);


            // Simpan detail
            if (!empty($detailItem)) {
                $detail = new TransaksiDetHelper($newItem);
                $detail->create($detailItem);
            }

            DB::commit();
            return [
                'status' => true,
                'data' => $newItem
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    /**
     * method untuk menginput data baru ke tabel t_transaksi
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $payload
     * $payload['Peminjaman] = string
     * $payload['user] = string
     * $payload['tanggal_mulai] = date
     * $payload['tanggal_selesai] = date
     *
     * @return void
     */
    public function createPengembalian(array $payload): array
    {
        try {
            DB::beginTransaction();
            $peminjamanId = $payload['peminjaman']['id'];
            $dataPeminjaman = $this->getById($peminjamanId);

            $tgl_pengembalian = Carbon::parse($payload['tanggal_pengembalian']);
            if ($tgl_pengembalian > $dataPeminjaman->tanggal_kembali) {
                $denda = $this->settingModel->getByParam('denda');
                $payload['jumlah_telat'] = $tgl_pengembalian->diffInDays($dataPeminjaman->tanggal_kembali);
                $payload['total_denda'] = $payload['jumlah_telat'] * $denda->value;
            }
            $payload['tanggal_pengembalian'] = $tgl_pengembalian;
            $payload['status'] = 'dikembalikan';
            $this->transaksiModel->edit($payload, $peminjamanId);

            $detail = new TransaksiDetHelper($dataPeminjaman);
            $detail->updateDel(false);

            DB::commit();
            return [
                'status' => true,
                'data' => $dataPeminjaman
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    /**
     * method untuk mengubah item pada tabel t_transaksi
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $payload
     * $payload['Peminjaman] = string
     * $payload['user] = string
     * $payload['tanggal_mulai] = date
     * $payload['tanggal_selesai] = date
     *
     * @return array
     */
    public function update(array $payload, int $id): array
    {
        try {
            $detailItem = $payload['buku'] ?? [];
            unset($payload['buku']);

            $dataPeminjaman = $this->getById($id);
            // $bukuLama = $this->bukuModel->getById($dataPeminjaman->m_buku_id);

            if (is_array($payload['user'])) {
                $payload['m_user_id'] = $payload['user']['id'];
                unset($payload['user']);
            }
            $parse = Carbon::parse($payload['tanggal_pinjam']);
            $payload['estimasi_kembali'] = $parse->addDays($payload['jumlah_hari_pinjam']);
            if ($payload['estimasi_kembali'] < Carbon::now()) {
                $denda = $this->settingModel->getByParam('denda');
                $payload['jumlah_telat'] = $payload['estimasi_kembali']->diffInDays(Carbon::now());
                $payload['total_denda'] = $payload['jumlah_telat'] * $denda->value;
            } else {
                $payload['jumlah_telat'] = null;
                $payload['total_denda'] = null;
            }
            $updatePeminjaman = $this->transaksiModel->edit($payload, $id);

            // Updatedetail
            if (!empty($detailItem)) {
                // $collectId = collect($detailItem)->pluck('id')->toArray();
                $detail = new TransaksiDetHelper($dataPeminjaman);
                $detail->update($detailItem);
            }
            return [
                'status' => true,
                'data' => $dataPeminjaman
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    /**
     * method untuk mengubah item pada tabel t_transaksi
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@gmail.com>
     *
     * @param array $payload
     * $payload['Peminjaman] = string
     * $payload['user] = string
     * $payload['tanggal_mulai] = date
     * $payload['tanggal_selesai] = date
     *
     * @return array
     */
    public function updatePengembalian(array $payload, int $id): array
    {
        try {
            $dataPeminjaman = $this->getById($id);
            $tgl_pengembalian = Carbon::parse($payload['tanggal_pengembalian']);
            if ($tgl_pengembalian > $dataPeminjaman->tanggal_kembali) {
                $denda = $this->settingModel->getByParam('denda');
                $payload['jumlah_telat'] = $tgl_pengembalian->diffInDays($dataPeminjaman->tanggal_kembali);
                $payload['total_denda'] = $payload['jumlah_telat'] * $denda->value;
            }
            $payload['tanggal_pengembalian'] = $tgl_pengembalian;
            $updatePeminjaman = $this->transaksiModel->edit($payload, $id);

            return [
                'status' => true,
                'data' => $dataPeminjaman
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'error' => $th->getMessage()
            ];
        }
    }

    /**
     * Menghapus data item dengan sistem "Soft Delete"
     * yaitu mengisi kolom deleted_at agar data tsb tidak
     * keselect waktu menggunakan Query
     *
     * @param  integer $id id dari tabel t_transaksi
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        try {
            $this->transaksiModel->drop($id);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
}
