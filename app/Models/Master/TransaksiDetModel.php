<?php

namespace App\Models\Master;

use App\Http\Traits\RecordSignature;
use App\Repository\ModelDetInterface;
use App\Repository\ModelInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TransaksiDetModel extends Model implements ModelDetInterface
{
    use SoftDeletes, RecordSignature, HasRelationships, HasFactory;

    /**
     * Menentukan nama tabel yang terhubung dengan Class ini
     *
     * @var string
     */
    protected $table = 't_transaksi_det';

    /**
     * Menentukan primary key, jika nama kolom primary key adalah "id",
     * langkah deklarasi ini bisa dilewati
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Akan mengisi kolom "created_at" dan "updated_at" secara otomatis,
     *
     * @var bool
     */
    public $timestamps = true;

    protected $attributes = [];

    protected $fillable = [
        't_transaksi_id',
        'm_buku_id',
    ];

    /**
     * Relasi ke BukuModel / tabel m_buku sebagai parentnya
     *
     * @return void
     */
    public function buku()
    {
        return $this->belongsTo(BukuModel::class, 'm_buku_id');
    }

    /**
     * Get the transaksi associated with the TransaksiDetModel
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transaksi(): HasOne
    {
        return $this->hasOne(TransaksiModel::class, 'id', 't_transaksi_id');
    }

    public function getAll(int $id): object
    {
        return $this->where('m_transaksi_id', $id)->get();
    }

    public function getById(int $id): object
    {
        return $this->find($id);
    }

    public function store(array $payload)
    {
        return $this->insert($payload);
    }

    public function edit(array $payload, int $id)
    {
        return $this->find($id)->update($payload);
    }

    public function drop(int $id)
    {
        return $this->find($id)->delete();
    }

    public function deleteUnUsed(array $unUsedId, int $parentId)
    {
        return $this->whereIn('id', $unUsedId)->where('t_transaksi_id', '=', $parentId)->delete();
    }
}
