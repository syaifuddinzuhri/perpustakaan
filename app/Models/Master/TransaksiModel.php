<?php

namespace App\Models\Master;

use App\Http\Traits\RecordSignature;
use App\Models\User\UserModel;
use App\Repository\ModelDetInterface;
use App\Repository\ModelInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TransaksiModel extends Model implements ModelInterface
{
    use SoftDeletes, RecordSignature, HasRelationships, HasFactory;

    /**
     * Menentukan nama tabel yang terhubung dengan Class ini
     *
     * @var string
     */
    protected $table = 't_transaksi';

    /**
     * Menentukan primary key, jika nama kolom primary key adalah "id",
     * langkah deklarasi ini bisa dilewati
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Akan mengisi kolom "created_at" dan "updated_at" secara otomatis,
     *
     * @var bool
     */
    public $timestamps = true;

    protected $attributes = [];

    protected $fillable = [
        'm_user_id',
        'no_invoice',
        'tanggal_pinjam',
        'estimasi_kembali',
        'tanggal_pengembalian',
        'jumlah_hari_pinjam',
        'jumlah_telat',
        'total_denda',
        'status',
    ];

    /**
     * Relasi ke UserModel / tabel m_buku sebagai parentnya
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(UserModel::class, 'm_user_id');
    }

    /**
     * Get all of the details for the TransaksiModel
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details(): HasMany
    {
        return $this->hasMany(TransaksiDetModel::class, 't_transaksi_id', 'id');
    }

    public function getAll(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        $dataItem = $this->query()->with(['user', 'details.buku', 'details.buku.rak', 'details.buku.kategori']);

        if (!empty($filter['status'])) {
            $dataItem->where('status', $filter['status']);
        }

        if (!empty($filter['m_user_id'])) {
            $dataItem->where('m_user_id', $filter['m_user_id']);
        }

        $sort = $sort ?: 'id DESC';
        $dataItem->orderByRaw($sort);
        $itemPerPage = $itemPerPage > 0 ? $itemPerPage : false;

        return $dataItem->paginate($itemPerPage)->appends('sort', $sort);
    }

    public function getById(int $id): object
    {
        return $this->with(['details', 'details.buku', 'user'])->find($id);
    }

    public function store(array $payload)
    {
        return $this->create($payload);
    }

    public function edit(array $payload, int $id)
    {
        return $this->find($id)->update($payload);
    }

    public function drop(int $id)
    {
        return $this->find($id)->delete();
    }
}
