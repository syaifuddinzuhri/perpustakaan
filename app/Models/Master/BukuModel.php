<?php

namespace App\Models\Master;

use App\Http\Traits\RecordSignature;
use App\Repository\ModelInterface;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class BukuModel extends Model implements ModelInterface
{
    use SoftDeletes, RecordSignature, HasRelationships, HasFactory;


    /**
     * Menentukan nama tabel yang terhubung dengan Class ini
     *
     * @var string
     */
    protected $table = 'm_buku';

    /**
     * Menentukan primary key, jika nama kolom primary key adalah "id",
     * langkah deklarasi ini bisa dilewati
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Akan mengisi kolom "created_at" dan "updated_at" secara otomatis,
     *
     * @var bool
     */
    public $timestamps = true;

    protected $attributes = [];

    protected $fillable = [
        'judul',
        'penulis',
        'penerbit',
        'tahun_terbit',
        'deskripsi',
        'isbn',
        'gambar',
        'jumlah',
        'status',
        'm_kategori_buku_id',
        'm_rak_buku_id'
    ];

    public function gambarUrl()
    {
        if (empty($this->gambar)) {
            return asset('assets/img/no-image.png');
        }

        return asset('storage/' . $this->gambar);
    }
    /**
     * Get the rak that owns the BukuModel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rak(): BelongsTo
    {
        return $this->belongsTo(RakBukuModel::class, 'm_rak_buku_id');
    }

    /**
     * Get the kategori that owns the BukuModel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kategori(): BelongsTo
    {
        return $this->belongsTo(KategoriBukuModel::class, 'm_kategori_buku_id');
    }

    public function getAll(array $filter, int $itemPerPage = 0, string $sort = ''): object
    {
        $dataItem = $this->query()->with(['rak', 'kategori']);

        if (!empty($filter['judul'])) {
            $dataItem->where('judul', 'LIKE', '%' . $filter['judul'] . '%');
        }

        if (!empty($filter['penulis'])) {
            $dataItem->where('penulis', 'LIKE', '%' . $filter['penulis'] . '%');
        }

        if (!empty($filter['penerbit'])) {
            $dataItem->where('penerbit', 'LIKE', '%' . $filter['penerbit'] . '%');
        }

        if (!empty($filter['tahun_terbit'])) {
            $dataItem->where('tahun_terbit', 'LIKE', '%' . $filter['tahun_terbit'] . '%');
        }

        if (!empty($filter['empty'])) {
            $dataItem->where('jumlah', '>', 0);
        }

        $sort = $sort ?: 'id DESC';
        $dataItem->orderByRaw($sort);
        $itemPerPage = $itemPerPage > 0 ? $itemPerPage : false;

        return $dataItem->paginate($itemPerPage)->appends('sort', $sort);
    }

    public function getById(int $id): object
    {
        return $this->query()->with(['rak', 'kategori'])->find($id);
    }

    public function store(array $payload)
    {
        return $this->create($payload);
    }

    public function edit(array $payload, int $id)
    {
        return $this->find($id)->update($payload);
    }

    public function drop(int $id)
    {
        return $this->find($id)->delete();
    }
}
