<?php

namespace App\Http\Controllers\Master;

use App\Helpers\Master\LaporanHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LaporanController extends Controller
{

    protected $laporan;

    public function __construct()
    {
        $this->laporan = new LaporanHelper();
    }

    public function laporanDashboard(Request $request)
    {
        $laporanDashboard = $this->laporan->laporanDashboard($request->all());
        return response()->success($laporanDashboard);
    }
}
