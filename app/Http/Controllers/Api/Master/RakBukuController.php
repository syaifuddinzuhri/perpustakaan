<?php

namespace App\Http\Controllers\Api\Master;

use App\Helpers\Master\RakBukuHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RakBuku\RakBukuResource;
use App\Http\Resources\RakBuku\RakBukuCollection;
use App\Http\Requests\RakBuku\CreateRequest;
use App\Http\Requests\RakBuku\UpdateRequest;
use App\Http\Resources\RakBuku\DetailResource;

class RakBukuController extends Controller
{
    private $rakBuku;

    public function __construct()
    {
        $this->rakBuku = new RakBukuHelper();
    }

    /**
     * Mengambil data rak buku dilengkapi dengan pagination
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function index(Request $request)
    {
        $filter = [
            'nama' => $request->nama ?? '',
        ];
        $rakBukus = $this->rakBuku->getAll($filter, $request['limit'] ?? 0, $request->sort ?? '');

        return response()->success(new RakBukuCollection($rakBukus));
    }

    /**
     * Membuat data rak buku baru & disimpan ke tabel rakBuku_auth
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function store(CreateRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/rakBuku/CreateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors());
        }

        $dataInput = $request->only(['nama']);
        $dataRakBuku = $this->rakBuku->create($dataInput);

        if (!$dataRakBuku['status']) {
            return response()->failed($dataRakBuku['error']);
        }

        return response()->success(new RakBukuResource($dataRakBuku['data']), 'Data rak buku berhasil disimpan');
    }

    /**
     * Menampilkan rakBuku secara spesifik dari tabel rakBuku_auth
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function show($id)
    {
        $dataRakBuku = $this->rakBuku->getById($id);

        if (empty($dataRakBuku)) {
            return response()->failed(['Data rak buku tidak ditemukan']);
        }

        return response()->success(new DetailResource($dataRakBuku));
    }

    /**
     * Mengubah data rak buku di tabel rakBuku_auth
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function update(UpdateRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/rakBuku/UpdateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors());
        }

        $dataInput = $request->only(['nama', 'id']);
        $dataRakBuku = $this->rakBuku->update($dataInput, $dataInput['id']);

        if (!$dataRakBuku['status']) {
            return response()->failed($dataRakBuku['error']);
        }

        return response()->success(new RakBukuResource($dataRakBuku['data']), 'Data rak buku berhasil disimpan');
    }

    /**
     * Soft delete data rak buku
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function destroy($id)
    {
        $dataRakBuku = $this->rakBuku->delete($id);

        if (!$dataRakBuku) {
            return response()->failed(['Mohon maaf data rak buku tidak ditemukan']);
        }

        return response()->success($dataRakBuku, 'Data rak buku telah dihapus');
    }
}
