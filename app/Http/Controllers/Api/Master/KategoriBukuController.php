<?php

namespace App\Http\Controllers\Api\Master;

use App\Helpers\Master\KategoriBukuHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\KategoriBuku\KategoriBukuResource;
use App\Http\Resources\KategoriBuku\KategoriBukuCollection;
use App\Http\Requests\KategoriBuku\CreateRequest;
use App\Http\Requests\KategoriBuku\UpdateRequest;
use App\Http\Resources\KategoriBuku\DetailResource;

class KategoriBukuController extends Controller
{
    private $kategoriBuku;

    public function __construct()
    {
        $this->kategoriBuku = new KategoriBukuHelper();
    }

    /**
     * Mengambil data kategori buku dilengkapi dengan pagination
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function index(Request $request)
    {
        $filter = [
            'nama' => $request->nama ?? '',
        ];
        $kategoriBuku = $this->kategoriBuku->getAll($filter, $request['limit'] ?? 0, $request->sort ?? '');

        return response()->success(new KategoriBukuCollection($kategoriBuku));
    }

    /**
     * Membuat data kategori buku baru & disimpan ke tabel
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function store(CreateRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/kategori buku/CreateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors());
        }

        $dataInput = $request->only(['nama']);
        $dataKategoriBuku = $this->kategoriBuku->create($dataInput);

        if (!$dataKategoriBuku['status']) {
            return response()->failed($dataKategoriBuku['error']);
        }

        return response()->success(new KategoriBukuResource($dataKategoriBuku['data']), 'Data kategori buku berhasil disimpan');
    }

    /**
     * Menampilkan kategori buku secara spesifik dari tabel
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function show($id)
    {
        $dataKategoriBuku = $this->kategoriBuku->getById($id);

        if (empty($dataKategoriBuku)) {
            return response()->failed(['Data kategori buku tidak ditemukan']);
        }

        return response()->success(new DetailResource($dataKategoriBuku));
    }

    /**
     * Mengubah data kategori buku di tabel
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function update(UpdateRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/kategori buku/UpdateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors());
        }

        $dataInput = $request->only(['nama', 'id']);
        $dataKategoriBuku = $this->kategoriBuku->update($dataInput, $dataInput['id']);

        if (!$dataKategoriBuku['status']) {
            return response()->failed($dataKategoriBuku['error']);
        }

        return response()->success(new KategoriBukuResource($dataKategoriBuku['data']), 'Data kategori buku berhasil disimpan');
    }

    /**
     * Soft delete data kategori buku
     *
     * @author Mochammad Syaifuddin Zuhri <mochammadsyaifuddinz@email.com>
     */
    public function destroy($id)
    {
        $dataKategoriBuku = $this->kategoriBuku->delete($id);

        if (!$dataKategoriBuku) {
            return response()->failed(['Mohon maaf data kategori buku tidak ditemukan']);
        }

        return response()->success($dataKategoriBuku, 'Data kategori buku telah dihapus');
    }
}
