<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Master\BukuHelper;
use App\Http\Requests\Buku\CreateRequest;
use App\Http\Requests\Buku\UpdateRequest;
use App\Http\Resources\Buku\BukuCollection;
use App\Http\Resources\Buku\BukuResource;
use App\Http\Resources\Buku\DetailResource;

class BukuController extends Controller
{
    protected $buku;

    public function __construct()
    {
        $this->buku = new BukuHelper();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [
            'judul' => $request->judul ?? '',
            'penulis' => $request->penulis ?? '',
            'penerbit' => $request->penerbit ?? '',
            'tahun_terbit' => $request->tahun_terbit ?? '',
            'empty' => $request->empty ?? '',
        ];
        $bukus = $this->buku->getAll($filter, $request['limit'] ?? 0, $request->sort ?? '');
        return response()->success(new BukuCollection($bukus));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/User/CreateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }

        $dataInput = $request->only([
            'judul',
            'penulis',
            'penerbit',
            'isbn',
            'tahun_terbit',
            'deskripsi',
            'gambar',
            'jumlah',
            'kategori',
            'rak'
        ]);
        $dataBuku = $this->buku->create($dataInput);

        if (!$dataBuku['status']) {
            return response()->failed($dataBuku['error'], 422);
        }

        return response()->success([], 'Data buku berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataBuku = $this->buku->getById($id);
        if (empty($dataBuku)) {
            return response()->failed(['Data buku tidak ditemukan']);
        }

        return response()->success(new DetailResource($dataBuku));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/User/UpdateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors());
        }

        $dataInput = $request->only([
            'id',
            'judul',
            'isbn',
            'penulis',
            'penerbit',
            'tahun_terbit',
            'deskripsi',
            'gambar',
            'jumlah',
            'kategori',
            'rak'
        ]);
        $dataBuku = $this->buku->update($dataInput, $dataInput['id']);

        if (!$dataBuku['status']) {
            return response()->failed($dataBuku['error']);
        }

        return response()->success(new BukuResource($dataBuku['data']), 'Data buku berhasil disimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataBuku = $this->buku->delete($id);

        if (!$dataBuku) {
            return response()->failed(['Mohon maaf data buku tidak ditemukan']);
        }

        return response()->success($dataBuku);
    }
}
