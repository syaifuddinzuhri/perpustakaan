<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Master\TransaksiHelper;
use App\Http\Requests\Transaksi\CreatePeminjamanRequest;
use App\Http\Requests\Transaksi\CreatePengembalianRequest;
use App\Http\Requests\Transaksi\CreateRequest;
use App\Http\Requests\Transaksi\UpdatePeminjamanRequest;
use App\Http\Requests\Transaksi\UpdatePengembalianRequest;
use App\Http\Requests\Transaksi\UpdateRequest;
use App\Http\Resources\Transaksi\TransaksiCollection;
use App\Http\Resources\Transaksi\TransaksiResource;
use App\Http\Resources\Transaksi\DetailResource;

class TransaksiController extends Controller
{
    protected $transaksi;

    public function __construct()
    {
        $this->transaksi = new TransaksiHelper();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [
            'status' => $request->status ?? '',
        ];
        $transaksi = $this->transaksi->getAll($filter, $request['limit'] ?? 20, $request->sort ?? '');
        return response()->success(new TransaksiCollection($transaksi));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTransaksiUser(Request $request)
    {
        $filter = [
            'm_user_id' => auth()->user()->id
        ];
        $transaksi = $this->transaksi->getTransaksiUser($filter, $request['limit'] ?? 20, $request->sort ?? '');
        return response()->success(new TransaksiCollection($transaksi));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePeminjamanRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/User/CreateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }

        $dataInput = $request->only([
            'buku',
            'user',
            'tanggal_pinjam',
            'jumlah_hari_pinjam',
        ]);
        $dataTransaksi = $this->transaksi->create($dataInput);

        if (!$dataTransaksi['status']) {
            return response()->failed($dataTransaksi['error'], 422);
        }

        return response()->success([], 'Data transaksi berhasil disimpan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePengembalian(CreatePengembalianRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/User/CreateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors(), 422);
        }

        $dataInput = $request->only([
            'peminjaman',
            'tanggal_pengembalian'
        ]);
        $dataTransaksi = $this->transaksi->createPengembalian($dataInput);

        if (!$dataTransaksi['status']) {
            return response()->failed($dataTransaksi['error'], 422);
        }

        return response()->success([], 'Data transaksi berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataTransaksi = $this->transaksi->getById($id);
        if (empty($dataTransaksi)) {
            return response()->failed(['Data transaksi tidak ditemukan']);
        }

        return response()->success(new DetailResource($dataTransaksi));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePeminjamanRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/User/UpdateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors());
        }

        $dataInput = $request->only([
            'id',
            'buku',
            'user',
            'tanggal_pinjam',
            'jumlah_hari_pinjam'
        ]);
        $dataTransaksi = $this->transaksi->update($dataInput, $dataInput['id']);

        if (!$dataTransaksi['status']) {
            return response()->failed($dataTransaksi['error']);
        }

        return response()->success(new TransaksiResource($dataTransaksi['data']), 'Data transaksi berhasil disimpan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePengembalian(UpdatePengembalianRequest $request)
    {
        /**
         * Menampilkan pesan error ketika validasi gagal
         * pengaturan validasi bisa dilihat pada class app/Http/request/User/UpdateRequest
         */
        if (isset($request->validator) && $request->validator->fails()) {
            return response()->failed($request->validator->errors());
        }

        $dataInput = $request->only([
            'id',
            'tanggal_pengembalian'
        ]);
        $dataTransaksi = $this->transaksi->updatePengembalian($dataInput, $dataInput['id']);

        if (!$dataTransaksi['status']) {
            return response()->failed($dataTransaksi['error']);
        }

        return response()->success(new TransaksiResource($dataTransaksi['data']), 'Data transaksi berhasil disimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataTransaksi = $this->transaksi->delete($id);

        if (!$dataTransaksi) {
            return response()->failed(['Mohon maaf data transaksi tidak ditemukan']);
        }

        return response()->success($dataTransaksi);
    }
}
