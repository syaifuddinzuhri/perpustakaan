<?php

namespace App\Http\Resources\Buku;

use Illuminate\Http\Resources\Json\JsonResource;

class BukuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'judul' => $this->judul,
            'deskripsi' => $this->deskripsi,
            'kategori' => $this->kategori,
            'gambar' => $this->gambar,
            'isbn' => $this->isbn,
            'gambarUrl' => $this->gambarUrl(),
            'jumlah' => $this->jumlah,
            'status' => $this->status,
            'penerbit' => $this->penerbit,
            'penulis' => $this->penulis,
            'tahun_terbit' => $this->tahun_terbit,
            'kategori' => $this->kategori,
            'rak' => $this->rak,
        ];
    }
}
