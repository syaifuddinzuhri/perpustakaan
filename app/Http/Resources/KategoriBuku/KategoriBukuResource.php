<?php

namespace App\Http\Resources\KategoriBuku;

use Illuminate\Http\Resources\Json\JsonResource;

class KategoriBukuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
