<?php

namespace App\Http\Resources\Transaksi;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'm_user_id' => $this->m_user_id,
            'no_invoice' => $this->no_invoice,
            'tanggal_pinjam' => $this->tanggal_pinjam,
            'estimasi_kembali' => $this->estimasi_kembali,
            'tanggal_pengembalian' => $this->tanggal_pengembalian,
            'jumlah_hari_pinjam' => $this->jumlah_hari_pinjam,
            'jumlah_telat' => $this->jumlah_telat,
            'total_denda' => $this->total_denda,
            'status' => $this->status,
            'user' => $this->user,
            'buku' => $this->details->pluck('buku'),
        ];
        // return parent::toArray($request);
    }
}
