<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'email' => $this->email,
            'fotoUrl' => $this->fotoUrl(),
            'updated_security' => $this->updated_security,
            'is_admin' => $this->role->is_admin,
            'akses' => $this->role->nama,
            'akses_menu' => json_decode($this->role->akses, TRUE)
        ];
    }
}
