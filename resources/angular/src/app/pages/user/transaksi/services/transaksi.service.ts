import { Injectable } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';

@Injectable({
    providedIn: 'root'
})
export class TransaksiService {
    constructor(private landaService: LandaService) { }

    getTransaksi(arrParameter) {
        return this.landaService.DataGet('/v1/transaksi-user', arrParameter);
    }

    getTransaksiById(transaksiId) {
        return this.landaService.DataGet('/v1/transaksi-user/' + transaksiId);
    }
}
