import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';
import { TransaksiService } from 'src/app/pages/master/transaksi/services/transaksi.service';

@Component({
    selector: 'app-detail-transaksi',
    templateUrl: './detail-transaksi.component.html',
    styleUrls: ['./detail-transaksi.component.scss']
})
export class DetailTransaksiComponent implements OnInit {
    @Input() transaksiId: number;

    detail: any

    constructor(
        private transaksiService: TransaksiService,
        private landaService: LandaService
    ) { }

    ngOnInit(): void {

    }

    ngOnChanges(changes: SimpleChange) {
        this.emptyForm();

    }

    emptyForm() {
        if (this.transaksiId > 0) {
            this.getTransaksiDetail(this.transaksiId);
        }
    }

    getTransaksiDetail(transaksiId) {
        this.transaksiService.getTransaksiById(transaksiId).subscribe((res: any) => {
            this.detail = res.data;
        }, (err: any) => {
            console.log(err);
        });
    }


    formatDate(date) {
        if (date) {
            const newDate = new Date(date);
            return newDate.toISOString().slice(0, 10);
        }
        return null
    }

    formatRupiah(angka) {
        if (angka) {
            var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            return split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        }
        return 0
    }

}
