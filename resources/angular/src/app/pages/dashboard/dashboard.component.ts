import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/services/auth.service';
import { DashboardService } from './services/dashboard.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    userLogin: any;
    listLaporanDashboard: any

    constructor(private authService: AuthService, private dashboardService: DashboardService, private router: Router,) { }

    ngOnInit(): void {
        if (this.authService.getToken() === '') {
            this.router.navigate(['/auth/login']);
        }
        this.authService.saveUserLogin()
        this.getLaporanDashboard();
        this.authService.getProfile().subscribe((user: any) => {
            this.userLogin = user;
        });
    }

    getLaporanDashboard() {
        this.dashboardService.getLaporanDashboard([]).subscribe((res: any) => {
            this.listLaporanDashboard = res.data
        }, (err: any) => {
            console.log(err);
        });
    }

    formatRupiah(angka) {
        if (angka) {
            var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            return split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        }
        return 0
    }

}
