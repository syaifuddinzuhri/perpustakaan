import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { LandaService } from 'src/app/core/services/landa.service';
import Swal from 'sweetalert2';
import { BukuService } from '../../services/buku.service';

@Component({
    selector: 'app-daftar-buku',
    templateUrl: './daftar-buku.component.html',
    styleUrls: ['./daftar-buku.component.scss']
})
export class DaftarBukuComponent implements OnInit {
    @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    dtOptions: DataTables.Settings = {};
    listBooks: [];
    titleCard: string;
    modelId: number;
    totalItems: number;
    isOpenForm: boolean = false;

    constructor(
        private bukuService: BukuService,
        private landaService: LandaService,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void {
        this.getBook()
    }

    getBook() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            searching: false,
            pagingType: "full_numbers",
            ajax: (dataTablesParameter: any, callback) => {

                const page = parseInt(dataTablesParameter.start) / parseInt(dataTablesParameter.length) + 1;
                const params = {
                    page: page
                };

                this.bukuService.getBuku(params).subscribe((res: any) => {
                    this.listBooks = res.data.list;
                    callback({
                        recordsTotal: res.data.meta.total,
                        recordsFiltered: res.data.meta.total,
                        data: []
                    });
                });
            },
        }
    }

    reload() {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.ajax.reload()
        });
    }

    showForm(show) {
        this.isOpenForm = show;
    }

    createBuku() {
        this.titleCard = 'Tambah Buku';
        this.modelId = 0;
        this.showForm(true);
    }

    updateBuku(bukuModel) {
        this.titleCard = 'Edit Buku : ' + bukuModel.nama;
        this.modelId = bukuModel.id;
        this.showForm(true);
    }

    deleteBuku(bukuId) {
        Swal.fire({
            title: 'Apakah kamu yakin ?',
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !',
        }).then((result) => {
            if (result.value) {
                this.bukuService.deleteBuku(bukuId).subscribe((res: any) => {
                    this.landaService.alertSuccess('Berhasil', res.message);
                    this.reload();
                }, err => {
                    console.log(err);
                });
            }
        });
    }

}
