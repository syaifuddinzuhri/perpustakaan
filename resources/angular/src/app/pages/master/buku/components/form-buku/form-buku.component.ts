import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';
import { KategoriBukuService } from '../../../kategori-buku/services/kategori-buku.service';
import { RakBukuService } from '../../../rak-buku/services/rak-buku.service';
import { BukuService } from '../../services/buku.service';

@Component({
    selector: 'app-form-buku',
    templateUrl: './form-buku.component.html',
    styleUrls: ['./form-buku.component.scss']
})
export class FormBukuComponent implements OnInit {

    @Input() bukuId: number;
    @Output() afterSave = new EventEmitter<boolean>();
    mode: string;
    formModel: {
        id: number,
        judul: string,
        penulis: string,
        penerbit: string,
        jumlah: number,
        tahun_terbit: number,
        deskripsi: string,
        gambar: string,
        isbn: string,
        gambarUrl: string,
        kategori: {
            id: number,
            nama: string
        },
        rak: {
            id: number,
            nama: string
        }
    };
    listYear: any
    gambarPreview: string
    listRakBuku: []
    listKategoriBuku: []

    constructor(
        private rakBukuService: RakBukuService,
        private kategoriBukuService: KategoriBukuService,
        private bukuService: BukuService,
        private landaService: LandaService
    ) { }

    ngOnInit(): void {
        this.listYear = this.getYears()
        this.getRakBuku()
        this.getKategoriBuku()
    }

    ngOnChanges(changes: SimpleChange) {
        this.emptyForm();
    }

    getRakBuku() {
        const params = {
            page: 1,
            limit: 100,
        }
        this.rakBukuService.getRakBuku(params).subscribe((res: any) => {
            this.listRakBuku = res.data.list;
        });
    }

    getKategoriBuku() {
        const params = {
            page: 1,
            limit: 100,
        }
        this.kategoriBukuService.getKategoriBuku(params).subscribe((res: any) => {
            this.listKategoriBuku = res.data.list;
        }, err => {
            console.log(err);
        });
    }

    getYears() {
        let startYear = 1900;
        let endYear = new Date().getFullYear();
        var years = [];
        for (var i = endYear; i >= startYear; i--) {
            years.push({
                value: i,
                text: i,
            });
        }
        return years;
    }

    fileChangeEvent(fileInput: any) {
        if (fileInput.target.files && fileInput.target.files[0]) {
            // Size Filter Bytes
            const max_size = 20971520;
            const allowed_types = ['image/png', 'image/jpeg'];
            const max_height = 15200;
            const max_width = 25600;
            const reader = new FileReader();
            reader.onload = (e: any) => {
                const image = new Image();
                image.src = e.target.result;
                image.onload = rs => {
                    const imgBase64Path = e.target.result;
                    this.gambarPreview = imgBase64Path;
                    this.formModel.gambar = imgBase64Path;
                };
            };

            reader.readAsDataURL(fileInput.target.files[0]);
        }
    }

    emptyForm() {
        this.mode = 'add';
        this.formModel = {
            id: 0,
            judul: '',
            penulis: '',
            penerbit: '',
            jumlah: null,
            tahun_terbit: null,
            deskripsi: '',
            isbn: '',
            gambar: '',
            gambarUrl: '',
            kategori: {
                id: 0,
                nama: ''
            },
            rak: {
                id: 0,
                nama: ''
            }
        }

        if (this.bukuId > 0) {
            this.mode = 'edit';
            this.getBuku(this.bukuId);
        }
    }

    getBuku(bukuId) {
        this.bukuService.getBukuById(bukuId).subscribe((res: any) => {
            this.formModel = res.data;
        }, err => {
            console.log(err);
        });
    }

    save() {
        if (this.mode == 'add') {
            this.bukuService.createBuku(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        } else {
            this.bukuService.updateBuku(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        }
    }

    back() {
        this.afterSave.emit();
    }
}
