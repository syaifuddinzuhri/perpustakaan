import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {
    NgbModule,
    NgbTooltipModule,
    NgbModalModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { MasterRoutingModule } from './master-routing.module';
import { DaftarUserComponent } from './users/components/daftar-user/daftar-user.component';
import { FormUserComponent } from './users/components/form-user/form-user.component';
import { DaftarRolesComponent } from './roles/components/daftar-roles/daftar-roles.component';
import { FormRolesComponent } from './roles/components/form-roles/form-roles.component';
import { DaftarRakBukuComponent } from './rak-buku/components/daftar-rak-buku/daftar-rak-buku.component';
import { FormRakBukuComponent } from './rak-buku/components/form-rak-buku/form-rak-buku.component';
import { DaftarKategoriBukuComponent } from './kategori-buku/components/daftar-kategori-buku/daftar-kategori-buku.component';
import { FormKategoriBukuComponent } from './kategori-buku/components/form-kategori-buku/form-kategori-buku.component';
import { FormBukuComponent } from './buku/components/form-buku/form-buku.component';
import { DaftarBukuComponent } from './buku/components/daftar-buku/daftar-buku.component';
import { DaftarPeminjamanComponent } from './transaksi/peminjaman/components/daftar-peminjaman/daftar-peminjaman.component';
import { DaftarPengembalianComponent } from './transaksi/pengembalian/components/daftar-pengembalian/daftar-pengembalian.component';
import { FormPengembalianComponent } from './transaksi/pengembalian/components/form-pengembalian/form-pengembalian.component';
import { FormPeminjamanComponent } from './transaksi/peminjaman/components/form-peminjaman/form-peminjaman.component';


@NgModule({
    declarations: [DaftarUserComponent, FormUserComponent, DaftarRolesComponent, FormRolesComponent, DaftarRakBukuComponent, FormRakBukuComponent, DaftarKategoriBukuComponent, FormKategoriBukuComponent, FormBukuComponent, DaftarBukuComponent, DaftarPeminjamanComponent, DaftarPengembalianComponent, FormPengembalianComponent, FormPeminjamanComponent],
    imports: [
        NgMultiSelectDropDownModule.forRoot(),
        CommonModule,
        MasterRoutingModule,
        NgbModule,
        NgbTooltipModule,
        NgbModalModule,
        NgSelectModule,
        FormsModule,
        DataTablesModule
    ]
})
export class MasterModule { }
