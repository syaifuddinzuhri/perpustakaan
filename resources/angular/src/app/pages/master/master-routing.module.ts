import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DaftarBukuComponent } from './buku/components/daftar-buku/daftar-buku.component';
import { DaftarKategoriBukuComponent } from './kategori-buku/components/daftar-kategori-buku/daftar-kategori-buku.component';
import { DaftarRakBukuComponent } from './rak-buku/components/daftar-rak-buku/daftar-rak-buku.component';
import { DaftarRolesComponent } from './roles/components/daftar-roles/daftar-roles.component';
import { DaftarPeminjamanComponent } from './transaksi/peminjaman/components/daftar-peminjaman/daftar-peminjaman.component';
import { DaftarPengembalianComponent } from './transaksi/pengembalian/components/daftar-pengembalian/daftar-pengembalian.component';
import { DaftarUserComponent } from './users/components/daftar-user/daftar-user.component';

const routes: Routes = [
    { path: 'users', component: DaftarUserComponent },
    { path: 'roles', component: DaftarRolesComponent },
    { path: 'rak-buku', component: DaftarRakBukuComponent },
    { path: 'kategori-buku', component: DaftarKategoriBukuComponent },
    { path: 'buku', component: DaftarBukuComponent },
    { path: 'transaksi-peminjaman', component: DaftarPeminjamanComponent },
    { path: 'transaksi-pengembalian', component: DaftarPengembalianComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MasterRoutingModule { }
