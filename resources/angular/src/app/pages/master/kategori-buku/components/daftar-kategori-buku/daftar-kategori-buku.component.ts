import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { LandaService } from 'src/app/core/services/landa.service';
import Swal from 'sweetalert2';
import { KategoriBukuService } from '../../services/kategori-buku.service';

@Component({
    selector: 'app-daftar-kategori-buku',
    templateUrl: './daftar-kategori-buku.component.html',
    styleUrls: ['./daftar-kategori-buku.component.scss']
})
export class DaftarKategoriBukuComponent implements OnInit {
    @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    dtOptions: DataTables.Settings = {};
    listKategoriBuku: [];
    titleCard: string;
    modelId: number;
    totalItems: number;
    isOpenForm: boolean = false;
    titleModal: string;

    constructor(
        private kategoriBukuService: KategoriBukuService,
        private landaService: LandaService,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void {
        this.getKategoriBuku()
    }

    getKategoriBuku() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            searching: false,
            pagingType: "full_numbers",
            ajax: (dataTablesParameter: any, callback) => {

                const page = parseInt(dataTablesParameter.start) / parseInt(dataTablesParameter.length) + 1;
                const params = {
                    page: page
                };

                this.kategoriBukuService.getKategoriBuku(params).subscribe((res: any) => {
                    this.listKategoriBuku = res.data.list;
                    callback({
                        recordsTotal: res.data.meta.total,
                        recordsFiltered: res.data.meta.total,
                        data: []
                    });
                });
            },
        }
    }

    showForm(show) {
        this.isOpenForm = show;
    }

    createKategoriBuku(modal) {
        this.titleModal = 'Tambah Rak Buku';
        this.modelId = 0;
        this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
    }

    updateKategoriBuku(modal, kategoriBukuModel) {
        this.titleModal = 'Edit : ' + kategoriBukuModel.nama;
        this.modelId = kategoriBukuModel.id;
        this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
    }

    deleteKategoriBuku(kategoriBukuId) {
        Swal.fire({
            title: 'Apakah kamu yakin ?',
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !',
        }).then((result) => {
            if (result.value) {
                this.kategoriBukuService.deleteKategoriBuku(kategoriBukuId).subscribe((res: any) => {
                    this.landaService.alertSuccess('Berhasil', res.message);
                    this.reload();
                }, err => {
                    console.log(err);
                });
            }
        });
    }

    reload() {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.ajax.reload()
        });
    }
}
