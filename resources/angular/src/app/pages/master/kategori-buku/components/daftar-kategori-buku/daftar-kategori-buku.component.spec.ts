import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarKategoriBukuComponent } from './daftar-kategori-buku.component';

describe('DaftarKategoriBukuComponent', () => {
  let component: DaftarKategoriBukuComponent;
  let fixture: ComponentFixture<DaftarKategoriBukuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarKategoriBukuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarKategoriBukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
