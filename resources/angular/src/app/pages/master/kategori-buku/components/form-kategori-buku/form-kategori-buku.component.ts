import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';
import { KategoriBukuService } from '../../services/kategori-buku.service';

@Component({
    selector: 'app-form-kategori-buku',
    templateUrl: './form-kategori-buku.component.html',
    styleUrls: ['./form-kategori-buku.component.scss']
})
export class FormKategoriBukuComponent implements OnInit {

    @Input() kategoriBukuId: number;
    @Output() afterSave = new EventEmitter<boolean>();
    mode: string;
    formModel: {
        id: number,
        nama: string
    }

    constructor(
        private kategoriBukuService: KategoriBukuService,
        private landaService: LandaService
    ) { }

    ngOnInit(): void {

    }

    ngOnChanges(changes: SimpleChange) {
        this.emptyForm();

    }

    emptyForm() {
        this.mode = 'add';
        this.formModel = {
            id: 0,
            nama: '',
        }
        if (this.kategoriBukuId > 0) {
            this.mode = 'edit';
            this.getKategoriBuku(this.kategoriBukuId);
        }
    }

    save() {
        if (this.mode == 'add') {
            this.kategoriBukuService.createKategoriBuku(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        } else {
            this.kategoriBukuService.updateKategoriBuku(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        }
    }

    getKategoriBuku(kategoriBukuId) {
        this.kategoriBukuService.getKategoriBukuById(kategoriBukuId).subscribe((res: any) => {
            this.formModel = res.data
        }, (err: any) => {
            console.log(err);
        });
    }

}
