import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormKategoriBukuComponent } from './form-kategori-buku.component';

describe('FormKategoriBukuComponent', () => {
  let component: FormKategoriBukuComponent;
  let fixture: ComponentFixture<FormKategoriBukuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormKategoriBukuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormKategoriBukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
