import { TestBed } from '@angular/core/testing';

import { KategoriBukuService } from './kategori-buku.service';

describe('KategoriBukuService', () => {
  let service: KategoriBukuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KategoriBukuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
