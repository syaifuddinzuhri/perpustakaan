import { Injectable } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';

@Injectable({
  providedIn: 'root'
})
export class KategoriBukuService {
    constructor(private landaService: LandaService) { }

    getKategoriBuku(arrParameter) {
        return this.landaService.DataGet('/v1/kategori-buku', arrParameter);
    }

    getKategoriBukuById(kategoriBukuId) {
        return this.landaService.DataGet('/v1/kategori-buku/' + kategoriBukuId);
    }

    createKategoriBuku(payload) {
        return this.landaService.DataPost('/v1/kategori-buku', payload);
    }

    updateKategoriBuku(payload) {
        return this.landaService.DataPut('/v1/kategori-buku', payload);
    }

    deleteKategoriBuku(kategoriBukuId) {
        return this.landaService.DataDelete('/v1/kategori-buku/' + kategoriBukuId);
    }
}
