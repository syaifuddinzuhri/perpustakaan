import { TestBed } from '@angular/core/testing';

import { RakBukuService } from './rak-buku.service';

describe('RakBukuService', () => {
  let service: RakBukuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RakBukuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
