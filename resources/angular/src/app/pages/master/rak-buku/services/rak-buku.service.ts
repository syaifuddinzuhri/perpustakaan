import { Injectable } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';

@Injectable({
  providedIn: 'root'
})
export class RakBukuService {
    constructor(private landaService: LandaService) { }

    getRakBuku(arrParameter) {
        return this.landaService.DataGet('/v1/rak-buku', arrParameter);
    }

    getRakBukuById(rakBukuId) {
        return this.landaService.DataGet('/v1/rak-buku/' + rakBukuId);
    }

    createRakBuku(payload) {
        return this.landaService.DataPost('/v1/rak-buku', payload);
    }

    updateRakBuku(payload) {
        return this.landaService.DataPut('/v1/rak-buku', payload);
    }

    deleteRakBuku(rakBukuId) {
        return this.landaService.DataDelete('/v1/rak-buku/' + rakBukuId);
    }
}
