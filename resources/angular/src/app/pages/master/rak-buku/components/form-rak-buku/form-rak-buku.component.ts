import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';
import { RakBukuService } from '../../services/rak-buku.service';

@Component({
  selector: 'app-form-rak-buku',
  templateUrl: './form-rak-buku.component.html',
  styleUrls: ['./form-rak-buku.component.scss']
})
export class FormRakBukuComponent implements OnInit {
    @Input() rakBukuId: number;
    @Output() afterSave = new EventEmitter<boolean>();
    mode: string;
    formModel: {
        id: number,
        nama: string
    }

    constructor(
        private rakBukuService: RakBukuService,
        private landaService: LandaService
    ) { }

    ngOnInit(): void {

    }

    ngOnChanges(changes: SimpleChange) {
        this.emptyForm();

    }

    emptyForm() {
        this.mode = 'add';
        this.formModel = {
            id: 0,
            nama: '',
        }
        if (this.rakBukuId > 0) {
            this.mode = 'edit';
            this.getRakBuku(this.rakBukuId);
        }
    }

    save() {
        if (this.mode == 'add') {
            this.rakBukuService.createRakBuku(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        } else {
            this.rakBukuService.updateRakBuku(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        }
    }

    getRakBuku(rakBukuId) {
        this.rakBukuService.getRakBukuById(rakBukuId).subscribe((res: any) => {
            this.formModel = res.data
        }, (err: any) => {
            console.log(err);
        });
    }

}
