import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRakBukuComponent } from './form-rak-buku.component';

describe('FormRakBukuComponent', () => {
  let component: FormRakBukuComponent;
  let fixture: ComponentFixture<FormRakBukuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRakBukuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRakBukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
