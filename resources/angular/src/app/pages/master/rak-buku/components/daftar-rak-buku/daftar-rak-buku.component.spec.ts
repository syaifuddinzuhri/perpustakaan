import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarRakBukuComponent } from './daftar-rak-buku.component';

describe('DaftarRakBukuComponent', () => {
  let component: DaftarRakBukuComponent;
  let fixture: ComponentFixture<DaftarRakBukuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarRakBukuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarRakBukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
