import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { LandaService } from 'src/app/core/services/landa.service';
import Swal from 'sweetalert2';
import { RakBukuService } from '../../services/rak-buku.service';

@Component({
    selector: 'app-daftar-rak-buku',
    templateUrl: './daftar-rak-buku.component.html',
    styleUrls: ['./daftar-rak-buku.component.scss']
})
export class DaftarRakBukuComponent implements OnInit {
    @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    dtOptions: DataTables.Settings = {};
    listRakBuku: [];
    titleCard: string;
    modelId: number;
    totalItems: number;
    isOpenForm: boolean = false;
    titleModal: string;

    constructor(
        private rakBukuService: RakBukuService,
        private landaService: LandaService,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void {
        this.getRakBuku()
    }

    getRakBuku() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            searching: false,
            pagingType: "full_numbers",
            ajax: (dataTablesParameter: any, callback) => {

                const page = parseInt(dataTablesParameter.start) / parseInt(dataTablesParameter.length) + 1;
                const params = {
                    page: page
                };

                this.rakBukuService.getRakBuku(params).subscribe((res: any) => {
                    this.listRakBuku = res.data.list;
                    callback({
                        recordsTotal: res.data.meta.total,
                        recordsFiltered: res.data.meta.total,
                        data: []
                    });
                });
            },
        }
    }

    showForm(show) {
        this.isOpenForm = show;
    }

    createRakBuku(modal) {
        this.titleModal = 'Tambah Rak Buku';
        this.modelId = 0;
        this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
    }

    updateRakBuku(modal, rakBukuModel) {
        this.titleModal = 'Edit : ' + rakBukuModel.nama;
        this.modelId = rakBukuModel.id;
        this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
    }

    deleteRakBuku(rakBukuId) {
        Swal.fire({
            title: 'Apakah kamu yakin ?',
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !',
        }).then((result) => {
            if (result.value) {
                this.rakBukuService.deleteRakBuku(rakBukuId).subscribe((res: any) => {
                    this.landaService.alertSuccess('Berhasil', res.message);
                    this.reload();
                }, err => {
                    console.log(err);
                });
            }
        });
    }
    reload() {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.ajax.reload()
        });
    }
}
