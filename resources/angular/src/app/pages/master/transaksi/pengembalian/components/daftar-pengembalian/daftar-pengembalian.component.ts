import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { LandaService } from 'src/app/core/services/landa.service';
import { TransaksiService } from '../../../services/transaksi.service';

@Component({
    selector: 'app-daftar-pengembalian',
    templateUrl: './daftar-pengembalian.component.html',
    styleUrls: ['./daftar-pengembalian.component.scss']
})
export class DaftarPengembalianComponent implements OnInit {
    @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    dtOptions: DataTables.Settings = {};
    listTransaksiPengembalian: [];
    titleCard: string;
    modelId: number;
    totalItems: number;
    isOpenForm: boolean = false;
    titleModal: string;


    constructor(
        private transaksiService: TransaksiService,
        private landaService: LandaService,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void {
        this.getTransaksiPengembalian()
    }

    reload() {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.ajax.reload()
        });
    }

    getTransaksiPengembalian() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            searching: false,
            pagingType: "full_numbers",
            ajax: (dataTablesParameter: any, callback) => {

                const page = parseInt(dataTablesParameter.start) / parseInt(dataTablesParameter.length) + 1;
                const params = {
                    status: 'dikembalikan',
                    page: page,
                };

                this.transaksiService.getTransaksi(params).subscribe((res: any) => {
                    this.listTransaksiPengembalian = res.data.list;
                    callback({
                        recordsTotal: res.data.meta.total,
                        recordsFiltered: res.data.meta.total,
                        data: []
                    });
                });
            },
        }
    }

    counter(i: number) {
        return new Array(i);
    }

    getDateNow() {
        return new Date().toJSON().slice(0, 10)
    }

    showForm(show) {
        this.isOpenForm = show;
    }

    createTransaksiPengembalian(modal) {
        this.titleModal = 'Tambah Pengembalian';
        this.modelId = 0;
        this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
    }

    updateTransaksiPengembalian(modal, pengembalianModel) {
        this.titleModal = 'Edit';
        this.modelId = pengembalianModel.id;
        this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
    }

    detail(modal, pengembalianModel) {
        this.titleModal = 'Detail Pengembalian';
        this.modelId = pengembalianModel.id;
        this.modalService.open(modal, { size: 'xl', backdrop: 'static' });
    }

    formatDate(date) {
        const newDate = new Date(date);
        return newDate.toISOString().slice(0, 10);
    }

    formatRupiah(angka) {
        if (angka) {
            var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            return split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        }
        return 0
    }
}
