import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPengembalianComponent } from './form-pengembalian.component';

describe('FormPengembalianComponent', () => {
  let component: FormPengembalianComponent;
  let fixture: ComponentFixture<FormPengembalianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPengembalianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPengembalianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
