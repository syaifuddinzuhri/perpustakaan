import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from '@angular/core';
import * as moment from 'moment';
import { LandaService } from 'src/app/core/services/landa.service';
import { BukuService } from 'src/app/pages/master/buku/services/buku.service';
import { UserService } from 'src/app/pages/master/users/services/user-service.service';
import { TransaksiService } from '../../../services/transaksi.service';

@Component({
    selector: 'app-form-pengembalian',
    templateUrl: './form-pengembalian.component.html',
    styleUrls: ['./form-pengembalian.component.scss']
})
export class FormPengembalianComponent implements OnInit {
    @Input() pengembalianId: number;
    @Output() afterSave = new EventEmitter<boolean>();
    mode: string;
    formModel: {
        id: number,
        tanggal_pengembalian: string,
        peminjaman: {
            id: number,
            no_invoice: string
        },
    };
    listTransaksiPeminjaman: []

    constructor(
        private bukuService: BukuService,
        private transaksiService: TransaksiService,
        private userService: UserService,
        private landaService: LandaService
    ) { }

    ngOnInit(): void {
        this.getTransaksiPeminjaman()
    }

    ngOnChanges(changes: SimpleChange) {
        this.emptyForm();

    }

    emptyForm() {
        this.mode = 'add';
        this.formModel = {
            id: 0,
            tanggal_pengembalian: null,
            peminjaman: {
                id: 0,
                no_invoice: ''
            },
        }
        if (this.pengembalianId > 0) {
            this.mode = 'edit';
            this.getPengembalian(this.pengembalianId);
        }
    }

    save() {
        if (this.mode == 'add') {
            this.transaksiService.createTransaksiPengembalian(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        } else {
            this.transaksiService.updateTransaksiPengembalian(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        }
    }

    getTransaksiPeminjaman() {
        const params = {
            status: 'dipinjam',
            page: 1
        }
        this.transaksiService.getTransaksi(params).subscribe((res: any) => {
            this.listTransaksiPeminjaman = res.data.list;
        }, (err: any) => {
            console.log(err);
        });
    }

    getPengembalian(pengembalianId) {
        this.transaksiService.getTransaksiById(pengembalianId).subscribe((res: any) => {
            // this.formModel = res.data
            this.formModel.id = res.data.id
            this.formModel.peminjaman = {
                id: res.data.id,
                no_invoice: res.data.no_invoice,
            }
            this.formModel.tanggal_pengembalian = moment(res.data.tanggal_pengembalian).format('YYYY-MM-DD')
        }, (err: any) => {
            console.log(err);
        });
    }

}
