import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarPengembalianComponent } from './daftar-pengembalian.component';

describe('DaftarPengembalianComponent', () => {
  let component: DaftarPengembalianComponent;
  let fixture: ComponentFixture<DaftarPengembalianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarPengembalianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarPengembalianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
