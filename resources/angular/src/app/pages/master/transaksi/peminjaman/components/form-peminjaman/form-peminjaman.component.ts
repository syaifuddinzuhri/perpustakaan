import { Component, EventEmitter, Input, OnInit, Output, SimpleChange, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { LandaService } from 'src/app/core/services/landa.service';
import { AuthService } from 'src/app/pages/auth/services/auth.service';
import { BukuService } from 'src/app/pages/master/buku/services/buku.service';
import { UserService } from 'src/app/pages/master/users/services/user-service.service';
import { TransaksiService } from '../../../services/transaksi.service';
import * as moment from 'moment';

@Component({
    selector: 'app-form-peminjaman',
    templateUrl: './form-peminjaman.component.html',
    styleUrls: ['./form-peminjaman.component.scss']
})
export class FormPeminjamanComponent implements OnInit {
    @Input() peminjamanId: number;
    @Output() afterSave = new EventEmitter<boolean>();
    dropdownListBuku = [];
    dropdownBukuSettings: IDropdownSettings = {};

    mode: string;
    formModel: {
        id: number,
        tanggal_pinjam: string,
        jumlah_hari_pinjam: number,
        buku: any,
        user: {
            id: number,
            nama: string,
        },
    };
    listBuku: []
    listUsers: []
    userLogin: any;

    constructor(
        private authService: AuthService,
        private bukuService: BukuService,
        private transaksiService: TransaksiService,
        private userService: UserService,
        private landaService: LandaService
    ) { }

    ngOnInit(): void {
        this.getBuku()
        this.getUser()
        this.dropdownBukuSettings = {
            idField: 'id',
            textField: 'judul',
            allowSearchFilter: true,
            enableCheckAll: true,
            selectAllText: 'Pilih semua buku',
            unSelectAllText: 'Reset semua pilihan',
            limitSelection: -1,
            clearSearchFilter: true,
            maxHeight: 197,
            closeDropDownOnSelection: false,
            showSelectedItemsAtTop: false,
            defaultOpen: false,
            itemsShowLimit: 3,
            searchPlaceholderText: 'Cari judul buku',
            noDataAvailablePlaceholderText: 'Buku tidak ada',
        };
    }

    ngOnChanges(changes: SimpleChange) {
        this.emptyForm();
    }

    onItemSelect(item: any) {
        this.formModel.buku.push(item.id)
    }
    onItemDeSelect(item: any) {
        this.removeItemAll(this.formModel.buku, item)
    }
    onSelectAll(items: any) {
        items.forEach(item => {
            this.formModel.buku.push(item.id)
        })
    }
    onUnSelectAll() {
        while (this.formModel.buku.length > 0) {
            this.formModel.buku.pop();
        }
    }

    removeItemAll(arr, value) {
        var i = 0;
        while (i < arr.length) {
            if (arr[i].id === value.id) {
                arr.splice(i, 1);
            } else {
                ++i;
            }
        }
        return arr;
    }

    emptyForm() {
        this.mode = 'add';
        this.formModel = {
            id: 0,
            tanggal_pinjam: null,
            jumlah_hari_pinjam: null,
            buku: [],
            user: {
                id: 0,
                nama: '',
            },
        }
        if (this.peminjamanId > 0) {
            this.mode = 'edit';
            this.getPeminjaman(this.peminjamanId);
        }
    }

    save() {
        if (this.mode == 'add') {
            this.transaksiService.createTransaksi(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        } else {
            let newBuku = [];
            this.formModel.buku.forEach(buku => {
                if (this.isObject(buku)) {
                    newBuku.push(buku.id)
                } else {
                    newBuku.push(buku)
                }
            })
            this.formModel.buku = newBuku;
            this.transaksiService.updateTransaksi(this.formModel).subscribe((res: any) => {
                this.landaService.alertSuccess('Berhasil', res.message);
                this.afterSave.emit();
            }, err => {
                this.landaService.alertError('Mohon Maaf', err.error.errors);
            });
        }
    }

    isObject(val) {
        return (typeof val === 'object');
    }

    getBuku() {
        const params = {
            page: 1,
            empty: true,
            limit: 100
        }
        this.bukuService.getBuku(params).subscribe((res: any) => {
            this.listBuku = res.data.list;
            this.dropdownListBuku = this.listBuku
        }, (err: any) => {
            console.log(err);
        });
    }

    getUser() {
        const params = {
            page: 1,
            roles: 2
        };
        this.userService.getUsers(params).subscribe((res: any) => {
            this.listUsers = res.data.list;
        }, (err: any) => {
            console.log(err);
        });
    }

    getPeminjaman(peminjamanId) {
        this.transaksiService.getTransaksiById(peminjamanId).subscribe((res: any) => {
            this.formModel = res.data
            this.formModel.tanggal_pinjam = moment(this.formModel.tanggal_pinjam).format('YYYY-MM-DD')
        }, (err: any) => {
            console.log(err);
        });
    }

}
