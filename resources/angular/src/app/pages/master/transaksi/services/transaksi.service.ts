import { Injectable } from '@angular/core';
import { LandaService } from 'src/app/core/services/landa.service';

@Injectable({
    providedIn: 'root'
})
export class TransaksiService {

    constructor(private landaService: LandaService) { }

    getTransaksi(arrParameter) {
        return this.landaService.DataGet('/v1/transaksi', arrParameter);
    }

    getTransaksiById(transaksiId) {
        return this.landaService.DataGet('/v1/transaksi/' + transaksiId);
    }

    createTransaksi(payload) {
        return this.landaService.DataPost('/v1/transaksi/peminjaman', payload);
    }

    updateTransaksi(payload) {
        return this.landaService.DataPut('/v1/transaksi/peminjaman', payload);
    }

    createTransaksiPengembalian(payload) {
        return this.landaService.DataPost('/v1/transaksi/pengembalian', payload);
    }

    updateTransaksiPengembalian(payload) {
        return this.landaService.DataPut('/v1/transaksi/pengembalian', payload);
    }

    deleteTransaksi(transaksiId) {
        return this.landaService.DataDelete('/v1/transaksi/' + transaksiId);
    }
}
